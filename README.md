# Searcher

## Build executable package
```
    mvn package
```

## Run
Example run:
```
java -jar searcher-1.0-SNAPSHOT.jar dir/subdir
```

## Design
1. path - list all files paths in given directory
2. extractor - extract text files content to *Text* object representation
3. rating - count file rating based on loaded file and given sentence
4. search - filter rating results
5. util - text normalization i.e. "Ala ma kota?!" -> "Ala ma kota"

## Potential future improvements
1. Extractors for other file types
2. Better handled IOExceptions, with more precisely messages and hints
3. Rating could printed as points amount i.e 4/7 and other ways
4. Mathematical operations on input handling i.e. *Result of 2 + 2 is 4*
