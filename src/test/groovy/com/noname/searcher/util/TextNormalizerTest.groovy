package com.noname.searcher.util

import spock.lang.Specification
import spock.lang.Unroll

class TextNormalizerTest extends Specification{

    @Unroll
    def"should return string with words separated by space"() {
        when:
        def results = TextNormalizer.normalize(phase)

        then:
        results == normalizedPhase

        where:
        phase                          | normalizedPhase
        "Ala ma 1 kota, a kot ma Alę." | "Ala ma 1 kota a kot ma Alę"
        "1 kot ma ma 4 Ale!!!"         | "1 kot ma ma 4 Ale"
        "!@#\$%%^&&Ala"                | "Ala"
    }
}
