package com.noname.searcher.rating

import com.noname.searcher.Text
import spock.lang.Specification
import spock.lang.Unroll

class SimpleRatingCounterTest extends Specification {

    @Unroll
    def "should count rating properly"() {
        given:
        def ratingCounter = new SimpleRatingCounter()

        when:
        def results = ratingCounter.count(targetText, loadedText)

        then:
        results == ratig

        where:
        targetText                    | loadedText                              | ratig
        new Text("Ala ma kota")       | new Text("Ala ma kota", "f1.txt")       | new Rating("f1.txt", 100L)
        new Text("Ala ma kota")       | new Text("Ala ma kota i psa", "f1.txt") | new Rating("f1.txt", 100L)
        new Text("Ala ma kota i psa") | new Text("Ala ma kota", "f1.txt")       | new Rating("f1.txt", 60L)
        new Text("Ala lubi placki")   | new Text("Ala ma kota", "f1.txt")       | new Rating("f1.txt", 33L)
        new Text("Ula lubi placki")   | new Text("Ala ma kota", "f1.txt")       | new Rating("f1.txt", 0L)

    }

}
