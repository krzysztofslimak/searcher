package com.noname.searcher.rating

import spock.lang.Specification
import spock.lang.Unroll

class RatingTest extends Specification {

    @Unroll
    def "should calculate value in percents by given rating and words count"() {
        when:
        def rating = new Rating("f1.txt", ratingValue, wordsCount)

        then:
        rating.getValueInPercents() == percents

        where:
        ratingValue | wordsCount | percents
        4           | 4          | 100L
        2           | 4          | 50L
        1           | 3          | 33L
    }

}
