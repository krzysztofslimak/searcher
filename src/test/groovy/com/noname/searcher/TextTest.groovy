package com.noname.searcher;

import spock.lang.Specification
import spock.lang.Unroll;

class TextTest extends Specification {

    @Unroll
    def "should count words occurrence"() {
        when:
        def text = new Text(sentence)

        then:
        text.getLength() == length
        text.getWordsOccurrence() == wordsOccurrence

        where:
        sentence             |length| wordsOccurrence
        "Ala ma kota."       |3     | ["ala": 1L, "ma":1L, "kota": 1L]
        "Ala ma kota i kota."|5     | ["ala": 1L, "ma":1L, "kota": 2L, "i": 1L]

    }
}
