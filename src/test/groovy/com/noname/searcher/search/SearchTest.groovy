package com.noname.searcher.search

import com.noname.searcher.Text
import com.noname.searcher.rating.Rating
import com.noname.searcher.rating.RatingCounter
import spock.lang.Specification

class SearchTest extends Specification {

    def ratingCounter = Mock(RatingCounter)
    def search = new Search(ratingCounter)

    def "should remove all 0 ratings"() {
        given:
        def texts = [new Text("ala"), new Text("ala")]

        and:
        2 * ratingCounter.count(_,_) >> new Rating("f1.txt", 0)

        when:
        def results = search.search("", texts)

        then:
        results == []
    }

}
