package com.noname.searcher.path

import spock.lang.Specification

class PathScannerTest extends Specification {
    def pathHelper = Stub(PathHelper)
    def pathScanner = new PathScanner(pathHelper)

    def "should throw exception if given path is null"() {
        when:
        pathScanner.getPaths(null)

        then:
        def exception = thrown(IllegalArgumentException)

        and:
        exception.message == "No path given to index."
    }
}
