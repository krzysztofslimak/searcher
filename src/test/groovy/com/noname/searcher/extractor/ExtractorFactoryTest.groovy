package com.noname.searcher.extractor

import spock.lang.Specification

import java.nio.file.Path

class ExtractorFactoryTest extends Specification {
    def path = Mock(Path)

    def "should create proper extractor for text file"() {
        given:
        path.toString() >> "directory/subdirectory/file.txt"

        when:
        def results = ExtractorFactory.create(path)

        then:
        results instanceof TextFileExtractor
    }

    def "should return null if there is no suitable extractor for given file type"() {
        given:
        path.toString() >> "directory/subdirectory/file.empty"

        when:
        def results = ExtractorFactory.create(path)

        then:
        results == null
    }
}
