package com.noname.searcher.extractor


import spock.lang.Specification

import java.nio.file.Path

class TextFileExtractorTest extends Specification {
    def "should concat all lines and set proper file name"() {
        given:
        def fileHelper = Mock(FileHelper)
        def path = Mock(Path)
        def fileExtractor = new TextFileExtractor(path, fileHelper)

        when:
        fileExtractor.extract()

        then:
        1 * fileHelper.readAllLines(path) >> ["ala ma kota", "i psa"]
        1 * path.getFileName() >> Mock(Path)
    }
}
