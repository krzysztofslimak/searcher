package com.noname.searcher.extractor;

import com.noname.searcher.Text;

import java.nio.file.Path;

public abstract class Extractor {
    protected final Path path;

    public Extractor(Path path) {
        this.path = path;
    }

    public abstract Text extract();
}
