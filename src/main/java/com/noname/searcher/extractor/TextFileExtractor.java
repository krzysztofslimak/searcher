package com.noname.searcher.extractor;

import com.noname.searcher.Text;

import java.io.IOException;
import java.nio.file.Path;

public class TextFileExtractor extends Extractor {

    private final FileHelper fileHelper;

    public TextFileExtractor(Path path) {
        super(path);
        this.fileHelper = new FileHelper();
    }

    public TextFileExtractor(Path path, FileHelper fileHelper) {
        super(path);
        this.fileHelper = fileHelper;
    }

    @Override
    public Text extract() {
        StringBuilder content = new StringBuilder();
        try
        {
            fileHelper.readAllLines(path).forEach(line -> content.append(line).append(" "));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return new Text(content.toString(), path.getFileName().toString());
    }
}
