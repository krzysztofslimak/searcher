package com.noname.searcher.extractor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class FileHelper {

    public List<String> readAllLines(Path path) throws IOException {
        return Files.readAllLines(path);
    }
}
