package com.noname.searcher.extractor;

import java.nio.file.Path;

public class ExtractorFactory {

    private static final String TEXT_FILE_EXTENSION = ".txt";

    // TODO: Other extractors could be created here
    public static Extractor create(Path path) {

        if(path.toString().endsWith(TEXT_FILE_EXTENSION)) {
            return new TextFileExtractor(path);
        }
        return null;
    }
}
