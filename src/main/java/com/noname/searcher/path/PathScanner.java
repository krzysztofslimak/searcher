package com.noname.searcher.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class PathScanner {
    private PathHelper pathHelper;

    public PathScanner() {
        this.pathHelper = new PathHelper();
    }

    public PathScanner(PathHelper pathHelper) {
        this.pathHelper = pathHelper;
    }

    public List<Path> getPaths(String relativePath) throws IOException {
        if (relativePath == null) {
            throw new IllegalArgumentException("No path given to index.");
        }

        // TODO: wrong path given NoSuchFileException could be handled here
        return pathHelper.walk(Paths.get(relativePath))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
    }

}
