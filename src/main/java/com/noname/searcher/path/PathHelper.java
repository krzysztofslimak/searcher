package com.noname.searcher.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class PathHelper {

    public Stream<Path> walk(Path path) throws IOException {
        return Files.walk(path);
    }
}
