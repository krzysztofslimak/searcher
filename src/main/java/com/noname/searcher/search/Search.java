package com.noname.searcher.search;

import com.noname.searcher.Text;
import com.noname.searcher.rating.Rating;
import com.noname.searcher.rating.RatingCounter;
import com.noname.searcher.rating.SimpleRatingCounter;

import java.util.List;
import java.util.stream.Collectors;

public class Search {

    private final int MAX_MATCHING_TEXTS = 10;
    private final RatingCounter ratingCounter;

    public Search(RatingCounter ratingCounter) {
        this.ratingCounter = ratingCounter;
    }

    public Search() {
        this.ratingCounter = new SimpleRatingCounter();
    }

    public List<Rating> search(String targetPhrase, List<Text> texts) {

        Text targetText = new Text(targetPhrase);

        List<Rating> ratings = texts.stream().map(text -> ratingCounter.count(targetText, text)).collect(Collectors.toList());

        return ratings.stream()
                .sorted(Rating::compareTo).filter(rating -> rating.getValueInPercents() > 0)
                .limit(MAX_MATCHING_TEXTS)
                .collect(Collectors.toList());
    }
}
