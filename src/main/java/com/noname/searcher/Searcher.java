package com.noname.searcher;

import com.noname.searcher.extractor.Extractor;
import com.noname.searcher.extractor.ExtractorFactory;
import com.noname.searcher.path.PathScanner;
import com.noname.searcher.rating.Rating;
import com.noname.searcher.search.Search;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Searcher {

    public static void main(String[] args) throws IOException {
        final Scanner keyboard = new Scanner(System.in);
        final String indexPath = args[0];
        final PathScanner pathScanner = new PathScanner();
        final Search search = new Search();

        List<Path> filesPath = pathScanner.getPaths(indexPath);
        List<Text> texts = filesPath.stream().map(ExtractorFactory::create)
                .filter(Objects::nonNull)
                .map(Extractor::extract)
                .collect(Collectors.toList());

        System.out.println(texts.size() + " files loaded in " + indexPath + " path");

        while (true) {
            System.out.print("search> ");
            final String line = keyboard.nextLine();
            final List<Rating> results = search.search(line, texts);

            results.forEach(System.out::println);
            if (results.isEmpty()) {
                System.out.println("No matching words found");
            }
        }
    }
}

