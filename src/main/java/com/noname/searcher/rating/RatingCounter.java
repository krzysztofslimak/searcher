package com.noname.searcher.rating;

import com.noname.searcher.Text;

public interface RatingCounter {
    Rating count(Text target, Text text);
}
