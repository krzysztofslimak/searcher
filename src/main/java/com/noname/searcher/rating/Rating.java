package com.noname.searcher.rating;

// TODO: Builder patter could be used here
public class Rating implements Comparable<Rating>{
    private final String fileName;
    private final Long valueInPercents;

    public Rating(String name, Long points, Long targetWordsCount) {
        this.fileName = name;
        this.valueInPercents = countAsPercents(points, targetWordsCount);
    }

    public Rating(String name, Long valueInPercents) {
        this.fileName = name;
        this.valueInPercents = valueInPercents;
    }

    public String getFileName() {
        return fileName;
    }

    public Long getValueInPercents() {
        return valueInPercents;
    }

    @Override
    public int compareTo(Rating rating) {
        return rating.valueInPercents.intValue() - this.valueInPercents.intValue();
    }

    @Override
    public String toString() {
        return fileName + " : " + valueInPercents + "%";
    }

    private Long countAsPercents(Long rating, Long searchedWordsCount) {
        return rating * 100 / searchedWordsCount;
    }
}
