package com.noname.searcher.rating;

import com.noname.searcher.Text;

public class SimpleRatingCounter implements RatingCounter {
    @Override
    public Rating count(Text targetText, Text loadedText) {
        Long ratingSummary = targetText.getWordsOccurrence().entrySet().stream()
                .map(entry -> {
                    final Long targetWordOccurrence = loadedText.getWordsOccurrence().getOrDefault(entry.getKey(), 0L);
                    return targetWordOccurrence > entry.getValue() ? entry.getValue() : targetWordOccurrence;
                })
                .reduce(0L, Long::sum);

        return new Rating(loadedText.getName(), ratingSummary, targetText.getLength());
    }
}
