package com.noname.searcher.util;

public class TextNormalizer {

    private final static int SPACE_ASCII_CODE = 32;

    public static String normalize(String text) {
        return text.chars()
                .filter(character -> Character.isLetterOrDigit(character) || character == SPACE_ASCII_CODE)
                .collect(StringBuilder::new,StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
