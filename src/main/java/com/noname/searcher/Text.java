package com.noname.searcher;

import com.noname.searcher.util.TextNormalizer;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Text {
    private final Map<String, Long> wordsOccurrence;
    private final String name;
    private final Long length;


    public Text(String content, String name) {
        this.name = name;
        this.wordsOccurrence = countOccurrenceOfWords(content);
        this.length = countLength();
    }

    public Text(String content) {
        this.name = null;
        this.wordsOccurrence = countOccurrenceOfWords(content);
        this.length = countLength();
    }

    public String getName() {
        return name;
    }

    public Map<String, Long> getWordsOccurrence() {
        return wordsOccurrence;
    }

    public Long getLength() {
        return length;
    }

    private Long countLength() {
        return wordsOccurrence.values().stream().reduce(0L, Long::sum);
    }

    private Map<String, Long> countOccurrenceOfWords(String content) {
        String[] words = content.toLowerCase().split(" ");

        return Stream.of(words)
                .map(TextNormalizer::normalize)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
